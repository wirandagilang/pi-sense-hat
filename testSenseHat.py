#!/usr/bin/python

#using python 3

from sense_hat import SenseHat
import random

sense = SenseHat()
sense.set_rotation(180)

# warna
red = (0, 0, 0)
R=0
G=0
B=0

while True:
  # ambil warna
  R = random.randrange(0, 255, 1)
  G = random.randrange(0, 255, 1)
  B = random.randrange(0, 255, 1)

  # tamil nilai warna
  print("RED:", R, "GREEN:", G, "BLUE:", B)

  red = (R, G, B)
  sense.show_message("HAI!", text_colour=red)
  